import urllib2
import simplejson

class ConceptNet:
    '''
    Query conceptnet to find synonyms for violence
    '''
    def __init__(self):
        '''
        Get the list of terms to use it
        '''
        self.list_of_terms = self.list_violence_terms()

    def list_violence_terms(self):
        '''
        List of terms used to query conceptnet for
        synonmns of violent tweets
        '''
        violence_terms = ['gun','terror','explode','crime',
                          'knife','bomb','suicide','police',
                          'violence','pain','fear','gang']
        return violence_terms

    def load_lookup_conceptnet_json(self, term):
        '''
        downloads the 10 nearest words for a term from the conceptnet
        api lookup feature
        '''
        c_request = 'http://conceptnet5.media.mit.edu/data/5.1/c/en/' + \
        '%s?limit=20'
        term_net = urllib2.Request(c_request %term)
	opener=urllib2.build_opener()
	term_f=opener.open(term_net)
	terms=simplejson.load(term_f)
	return [terms['edges'][k]['startLemmas']
                for k in range(len(terms['edges']))]

    def load_association_conceptnet_json(self, term):
        '''
        downloads the 10 nearest terms for a term from the conceptnet
        api association feature
        '''
        c_request = 'http://conceptnet5.media.mit.edu/data/5.1/c/en/' + \
        '%s?limit=1000' 
        term_net = urllib2.Request(c_request %term)
	opener=urllib2.build_opener()
	term_f=opener.open(term_net)
	terms=simplejson.load(term_f)
	return [terms['edges'][k]['startLemmas']
                for k in range(len(terms['edges']))]

    def create_term_dict(self): # Pass on this for now
        '''
        creates a dictionary in which the keys are terms from a list
        of terms and their values are a deduplicated list of 'edges' 
	returned by concept net
        '''
        term_dict={term: list(set(load_lookup_conceptnet_json(term)
                                  +load_association_conceptnet_json(term)))
                   for term in list_of_terms}
	return term_dict

    def concept_dict(self):
        '''
        creates a dictionary in which the keys are terms from a list
        of terms and their values are a deduplicated list of 'edges' 
	returned by concept net
        '''
        term_dict = {}
        for term in self.list_of_terms:
            syn_list = []
            for synonym in self.load_lookup_conceptnet_json(term):
                syn_list.append(synonym)
            term_dict[term] = syn_list
        return term_dict
                              

cn = ConceptNet()
data = cn.concept_dict()
try:
    print data.keys(), len(data.keys())
    print "\ncrime\n"
    for num in data['crime']:
        print num
    print "\nsuicide\n"
    for i in data['suicide']:
        print i
    print "ok it's working"
except:
    print "not working"
