
#pakathon
import urllib2
import simplejson

def load_lookup_conceptnet_json(term):
	'''downloads the 10 nearest words for a term from the conceptnet api lookup feature'''
	term_net = urllib2.Request('http://conceptnet5.media.mit.edu/data/5.1/c/en/%s?limit=20' %term)
	opener=urllib2.build_opener()
	term_f=opener.open(term_net)
	terms=simplejson.load(term_f)
	return [terms['edges'][k]['startLemmas'] for k in range(len(terms['edges']))]

def load_association_conceptnet_json(term):
	'''downloads the 10 nearest terms for a term from the conceptnet api association feature'''
	term_net = urllib2.Request('http://conceptnet5.media.mit.edu/data/5.1/c/en/%s?limit=1000' %term)
	opener=urllib2.build_opener()
	term_f=opener.open(term_net)
	terms=simplejson.load(term_f)
	return [terms['edges'][k]['startLemmas'] for k in range(len(terms['edges']))]

def create_term_dict(list_of_terms):
	'''creates a dictionary in which the keys are terms from a list of terms and their values are a deduplicated list of 'edges' 
	returned by concept net''' 
	term_dict={term: list(set(load_lookup_conceptnet_json(term)+load_association_conceptnet_json(term))) for term in list_of_terms}
	return term_dict

	
	
