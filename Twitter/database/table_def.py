__author__ = 'ben'
# table_def.p
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, DateTime, Integer, String, Float, UnicodeText, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship , backref, sessionmaker
import sqlite3
#engine = create_engine('sqlite://',
#                connect_args={'detect_types': sqlite3.parse_decltypes|sqlite3.parse_colnames},
#                native_datetime=true
#                )

engine = create_engine('sqlite:///tweet_test.db',
    connect_args={'detect_types': sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES},
    native_datetime=True
    ,
                echo=False
)

Session = sessionmaker(bind=engine)
Base = declarative_base()

#id = Column(Integer, primary_key=True)
#email_address = Column(String, nullable=False)
#user_id = Column(Integer, ForeignKey('users.id'))
#
#user = relationship("User", backref=backref('addresses', order_by=id))


tweet_cities = Table('tweet_cities', Base.metadata,
    Column('city_name', Integer, ForeignKey('cities.name')),
    Column('tweet_id', Integer, ForeignKey('tweets.id'))
)


class City(Base):
    __tablename__ = "cities"
    name = Column(String, primary_key=True)
    latitude = Column(Float)
    longitude = Column(Float)
    earliest_tweet_id = Column(Integer)
    latest_tweet_id = Column(Integer)
    radius = Column(Float)

    def __repr__(self):
        return "<City('%s')>" % (self.name.encode('utf8'))


class Tweet(Base):
    """"""
    __tablename__ = "tweets"

    id = Column(Integer, primary_key=True)
    latitude = Column(Float)
    longitude = Column(Float)
    text = Column(UnicodeText)
    date = Column(DateTime)

    #city = Column(String)
    #, ForeignKey('cities.name'))
    cities = relationship("City", secondary=tweet_cities, backref=backref('tweets', order_by=date))

    def __repr__(self):
        return "<Tweet('%s')>" % (self.text.encode('utf8'))

def create_tables():
    Base.metadata.create_all(engine)
