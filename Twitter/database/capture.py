from Twitter.get_tweets.twitter_scraper import TwitterScraper
import sqlite3
import re
from metanl import general

class CaptureFeeds:
    '''
    This class takes Tweets and puts them into a sqlite3 database.
    This was originally written for RSS feeds so please excuse that
    terminology here. 
    '''
    def __init__(self):
        # Initialize sqlite3
        self.conn = sqlite3.connect("Tweets.db")
        self.c = self.conn.cursor()

    def initial_db(self):
        '''
        Sets up db tables where each RSS link feeds
        into a separate table because it's easier
        to aggregate then deaggregate.
        '''
        # Make table name match RSS feed name
        # d = feedparser.parse(RSS_link)
        table_name = "Pak_Table"#re.sub(r'\W+', '', d.feed.title)
        # Creating string separately makes multiple table 
        # creation easier
        table = "CREATE TABLE " +  table_name + \
                "( primary_key text, twit_text text, created_at text)" 
        # Create table in sqlite3
        self.c.execute(table)
        # Which tables are being entered?
        print "\t" + table_name 
        # Save (commit) the changes
        self.conn.commit()
        # Close the connection to sqlite3
        print "\n initial_db is complete"
    
    def get_tweets(self):
        '''
        This gets tweets for a certain query then
        puts them in a dictionary.
        '''
        search_geo = search(geocode=near('clifton',50))
        return search_geo

    def get_tablenames(self):
        ''' Table names are cleaned for SQL queries'''
        # List of tables names that can be queried
        table_name = "Pak_Table" #self.c.fetchall()
        return table_name # This is revised for using only one table

    def insert_query(self):
        '''
        Create a list of queries to run that
        include the name of each table 
        '''
        table_name = self.get_tablenames()
        insert_queries = "INSERT INTO " + table_name + \
                          " VALUES (?,?,?)" 
        return insert_queries

    def preprocess_text(self, text):
        '''
        Given any basestring as input, make its representation
        consistent:
        - Ensure that it is a Unicode string, converting from
        UTF-8 if necessary.
        - Detect whether the text was incorrectly encoded into UTF-8
        and fix it, as defined in `fix_bad_unicode`.
        - Replace HTML entities with their equivalent characters.
        - Replace newlines and tabs with spaces.
        - Remove all other control characters.
        - Normalize it with Unicode normalization form KC, which
        applies the following relevant transformations:
        - Combine characters and diacritics that are written using
        separate code points, such as converting "e" plus an acute
        accent modifier into "", or converting "ka" plus a
        dakuten into the single character "ga".
        - Replace characters that are functionally equivalent with the
        most common form: for example, half-width katakana will be
        replaced with full-width, and full-width Roman characters will
        be replaced with ASCII characters.
        '''
        return general.preprocess_text(text)

    def articles(self): # change the fields based on the twitter api
        '''                  
        This should give me a list of tuples containing
        information on the articles for a given RSS feed
        '''
        tweets = self.get_tweets()
        new_list = []
        for tweet in range(len(tweets)):
            # Hack simpleflake for sqlite3
            primary_key = str(tweets[tweet]['id']) 
            # Remaining columns are iterated from feed parse
            a_text = tweets[tweet]['text']
            twit_text = self.preprocess_text(a_text)
            created_at = tweets[tweet]['created_at']
            new_list.append((primary_key,twit_text,created_at))
        return new_list

    def table(self):
        '''
        This should be a dictionary of tables where each table
        consists of a list of tuples. 
        '''
        check = self.articles()
        table_dict = {self.get_tablenames(): self.articles()}
        return table_dict

    def populate_db(self):
        ''' 
        Queries are matched with dict keys which then
        provides the values associated with each query by sharing
        the table names as a reference point. This allows rows to be 
        populated for each table leading to the population of the db.
        '''
        #self.c.executemany(self.insert_query(),self.articles())
        self.c.executemany(self.insert_query(), self.articles())
        self.conn.commit()
        print "\n populate_db is complete"

    def rm_duplicates(self): # Change what you group the duplicate query on
        '''
        Limitation of this duplicate removal approach is that only one
        duplicate entry will be removed (containing the lowest valued
        primary_key). If the number of  duplicate entries per item  > 2
        then that will introduce a bug. 
        '''
        # Remove duplicate queries
        for table_name in self.table().keys():
            query = "DELETE FROM " + table_name + " WHERE primary_key " \
                    "NOT IN " \
                    "(SELECT min(primary_key) FROM " + table_name + \
                    " GROUP BY twit_text, created_at);"
            self.c.execute(query)
            self.conn.commit()
        self.conn.close()
        print " rm_duplicates is complete"


capture = CaptureFeeds()
try:
    capture.initial_db()
    capture.populate_db()
    print "ok it has a pulse"
except:
    capture.populate_db()
    print "ok it has a pulse"
