import nose
from gensim import corpora, models

# nlp_lsi classes imported for testing
from Twitter.nlp_lsi import PrepareCorpus, TransformCorpus, SimilarityQueries 

class TestPrepareCorpus:
    '''
    Tests to see if the PrepareCorpus class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def test_create_document_for_lsi(self):
        '''
        Check to see if data is being loaded from Twitter and
        see how long it's taking
        '''
        pass

    def test_data_structure_create_document_for_lsi(self):
        """
        Check data structure for create_document_for_lsi. It should
        return a list where each list item is a string
        """
        prepare_corpus = PrepareCorpus()
        assert type(prepare_corpus.create_document_for_lsi())\
            == list
        assert type(prepare_corpus.create_document_for_lsi()[0])\
            == list

    def test_save_dict(self):
        '''
        Check to see if dictionary saved to disk
        '''
        path = "/home/ty/code/pakathon/Twitter/" 
        assert corpora.Dictionary.load(path + 'deerwester.dict')

    def test_vector_convert(self):
        '''
        See if the converted document saved to disk as .mm
        '''
        path = "/home/ty/code/pakathon/Twitter/" 
        assert corpora.MmCorpus(path + 'deerwester.mm')


class TestTransformCorpus:
    '''
    Tests to see if the TransformCorpus class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def test_vectorize_corpus(self):
        '''
        Was the whole corpus vectorized?
        '''
        transform_corpus = TransformCorpus()
        assert transform_corpus.vectorize_corpus()

    def test_lsi_transform(self):
        '''
        Did the lsi model transform properly?
        '''
        transform_corpus = TransformCorpus()
        assert transform_corpus.lsi_transform()

    def test_model_persistency(self):
        '''
        Did the lsi model save properly?
        '''
        path = "/home/ty/code/pakathon/Twitter/"
        assert models.LsiModel.load(path + 'model.lsi')


class TestSimilarityQueries:
    '''
    Tests to see if the TransformCorpus class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def __init__(self):
        self.similarity_queries = SimilarityQueries()

    def test_user_query(self):
        '''
        Make sure query is vectorized
        '''
        assert self.similarity_queries.user_query()

    def test_init_query_structure(self):
        '''
        Make sure it loads
        '''
        assert self.similarity_queries.init_query_structure()

    def test_perform_query(self):
        '''
        Make sure it loads
        '''
        assert type(self.similarity_queries.perform_query())

    def test_sort_similar(self):
        '''
        Make sure it loads
        '''
        assert self.similarity_queries.sort_similar()

    def test_match_tweet_to_sim(self):
        '''
        Make sure it loads
        '''
        assert self.similarity_queries.match_tweet_to_sim()

    





