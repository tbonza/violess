__author__ = 'ben'
from time import sleep
from get_tweets.twitter_scripts import *
from database.table_def import Session, Tweet, City
from get_tweets.util import twitter_to_datetime, YMD_to_datetime, datetime_to_YMD
import time

from util import get_logger

logger = get_logger('twitter_scraper')

class TwitterScraper(object):

    def __init__(self):
        self.db = Session()
        self.cities = self.db.query(City).all()

    def set_city(self, city):
        if isinstance(city, basestring):
            city = self.db.query(City).filter_by(name=city).first()
        self.city = city
        self.city_name = city.name
        self.geocode = (city.latitude, city.longitude, "%.3fmi" % city.radius)
        self.radius = city.radius
        self.location = city.name

    def num_tweets(self):
        for city in self.db.query(City).all():
            self.set_city(city)
            print city.name, len(self.get_stored_tweets())




    def get_all_data(self, city, *args, **kwargs):
        '''
        Pulls all available tweets for the current city from twitter
        '''
        self.set_city(city)
        first = self.search(count=1, *args, **kwargs)
        max_id = first[0]['id']
        self.city.latest_tweet_id = max_id
        self.db.commit()

        success = False
        d = []
        count = 0
        while (True):
            count += 1
            # TODO Note to self, work out store_only inconsistencies - Ben
            d, success, max_id = self.time_search(max_id=max_id, first_id=0, store_only=False, *args, **kwargs)
            print max_id, len(d)
            if len(d) <= 4:
                break

        try:
            self.city.earliest_id = d[-1]['id']
        except Exception:
            print "derp"
        self.db.commit()


    def seed(self):
        for city in self.cities:
            self.set_city(city)
            results, _, _ = self.id_search(max_iterations=1)
            if results:
                max_id = results[0]['id']
                print "Got max_id ", max_id
                self.city.latest_tweet_id = max_id
            self.db.commit()


    def update(self):
        for city in self.cities:
            self.set_city(city)
            results, _, _ = self.id_search(first_id=self.city.latest_tweet_id, max_iterations=100000)
            if results:
                max_id = results[0]['id']
                print "Got max_id ", max_id
                self.city.latest_tweet_id = max_id
            self.db.commit()

        pass


    def time_search(self, min_date=None, max_date=None, first_id=None, max_id=None, store_only=False, *args, **kwargs):
        '''
        Pulls tweets between given dates or within given tweet ids
        (tweet1.id > tweet2.id) -> (tweet1.date > tweet2.date)

        keep chunk_size <= 100 to prevent twitter from getting upset

        this is kinda messy right now, apologies - Ben
        '''

        if first_id is None:
            if isinstance(min_date, basestring):
                min_date = YMD_to_datetime(min_date)

        if max_id is None:
            if isinstance(max_date, basestring):
                max_date = YMD_to_datetime(max_date)

        if first_id is None:
            first = self.search(until=datetime_to_YMD(min_date), store=False, count=1, *args, **kwargs)
            if not first:
                print "no first"
                return
            first_id = first[0]['id']


        if max_id is None:
            last = self.search(until=datetime_to_YMD(max_date), store=False, count=1, *args, **kwargs)
            if not last:
                print "no last"
                return
            last_date = twitter_to_datetime(last['date'])
            max_id = int(last[0]['id'])

            # A litle awkard to detect this, but I believe all other ways
            # fail on edge cases
            if last_date <= min_date:
                print "No tweets with those parameters in that time range"
                return
        return self.id_search(first_id=first_id, max_id=max_id, store_only=store_only, *args, **kwargs)



    def id_search(self, first_id=None, max_id=None, store_only=False, chunk_size=90, max_iterations=30, *args, **kwargs):

        # Using chunks in case each search has a count limit (there is a limit, don't exceed 100 chunk size)

        all_results = []

        current_iteration = 0
        success = False
        while(True):
            current_iteration += 1

            results = self.search(since_id=first_id, max_id=max_id, count=chunk_size, *args, **kwargs)
            if not store_only:
                all_results += results

            if 0 < len(results) <= chunk_size:


                max_id = int(results[-1]['id']) - 1
            elif len(results) > chunk_size:
                print "Incorrect result count"
                raise Exception("got more results than expected in time_search")
            else:
                success = True
                # We should have gotten all the results. Yay!
                break

            if current_iteration >= max_iterations:
                print "time_search did not get all results"
                success = False
                break

        return all_results, success, max_id



    def get_stored_tweets(self):
        return self.db.query(Tweet).filter_by(city=self.city_name).all()

    def get_danger(self):
        DANGER_WORDS = ['terror', 'bomb', 'explosion', 'violence', 'killing', 'bang', 'pow', 'fire', 'gang', 'injured', 'burning',
                        'shooting', 'RATM', 'murder', 'randsom', 'assassination', 'gunshot', 'rape']


        #city_set = self.db.query(Tweet).filter_by(city=self.city_name).all()
        city_set = self.db.query(Tweet).filter(Tweet.cities.contains(self.city)).all()
        total_tweets = len(city_set)
        print self.city.name, " num tweets ", total_tweets
        tweet_texts = [tweet.text for tweet in city_set]

        danger_tweets = 0
        for text in tweet_texts:
            for word in DANGER_WORDS:
                if word in text:
                    danger_tweets += 1

        return danger_tweets * 1.0 / total_tweets



    def search(self, store=True, *args, **kwargs):
        tweets = search(geocode=self.geocode, *args, **kwargs)
        for tweet in tweets:
            tweet['id'] = tweet.pop('id')
            tweet['date'] = twitter_to_datetime(tweet.pop('created_at'))
            #print tweet
        if store:
            for tweet in tweets:
            #    tweet['id'] = tweet.pop('id')
            #    tweet['date'] = tweet.pop('created_at')

                tweet_object = self.db.query(Tweet).filter_by(id=tweet['id']).first()
                if tweet_object:
                    if self.city not in tweet_object.cities:
                        tweet_object.cities.append(self.city)
                        print "Added new city to existing tweet"
                    # Add current city if it is not already associated with this tweet
                    print "Avoided duplicate entry with id", tweet['id']
                else:
                    tweet_object = Tweet(**tweet)
                    tweet_object.cities.append(self.city)
                    self.db.add(tweet_object)
            self.db.commit()
        return tweets




def daemon():
    logger.info('Starting daemon')
    ts = TwitterScraper()
    while(True):
        print ("running in 5")
        sleep(5)
        print("running")
        try:
            ts.update()
            logger.info('scraped')
        except Exception:
            logger.warning('timeout in daemon (or other exception)')
            print "Caught an exception, hopefully it was a rate limit one"
        print("sleeping for 5 minutes")
        sleep(60*5)





