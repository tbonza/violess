__author__ = 'ben'
import logging

logging.basicConfig(format='%(asctime)s (%(name)s): %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG,
                    filename='violess.log')

def get_logger(name=''):
    logger = logging.getLogger(name)
    return logger