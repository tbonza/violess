
from database.table_def import create_tables, Session
from database.table_def import City
from twitter_scraper import TwitterScraper
import subprocess

def sh(c):
    subprocess.call(c,shell=True)


city_data = {
    'clifton' : (24.826937,67.031179, 5),
    'saddar' : (24.853344, 67.016206, 5),
    'karachi' : (24.8600, 67.010, 20),
    'landhi' : (24.84999, 67.19988, 10),
    'kamlapur' : (22.143528, 71.198165, 20),
    'gondal': (21.958967, 70.796478, 20)
}



def all_the_things2():
    sh('rm tweet_test.db')
    reset()
    data = {}
    ts = TwitterScraper()
    for city in city_data:
        ts.get_all_data(city)
        data[city] = ts.get_danger()
        #score = city_script()
        #print city, city_script(city)
    return data

def all_the_things():
    data = {}
    for city in city_data:
        sh('rm tweet_test.db')
        data[city] = city_script(city)
        #score = city_script()
        #print city, city_script(city)
    return data


def reset():
    create_tables()
    s = Session()
    for city in city_data:
        data = city_data[city]
        c = City(name=city, latitude=data[0], longitude=data[1], radius=data[2])
        s.add(c)
    s.commit()

def city_script(cityname):
    reset()
    ts = TwitterScraper()
    ts.get_all_data(cityname)
    return ts.get_danger()



def reset2():
    reset()
    ts = TwitterScraper()
    for city in city_data:
        #ts.set_city(city)
        ts.get_all_data(city)
        ts.search()




