import sqlite3

class ScoreTweets:
    '''
    Selects Tweets which have a preselected hashtag
    '''
    def __init__(self):
        # Path
        self.db_path = "/home/ty/code/test_raspy-aspie/"
        self.txt_path = "/home/ty/code/pakathon/keywordlists/"
        # Open sqlite3 db
        self.conn = sqlite3.connect(self.db_path + 'tweet_test.db')
        self.c = self.conn.cursor()
        # Preselected hashtags
        self.hash_list = [elt.strip() for elt in \
                          file(self.txt_path + \
                               "hashtags.txt", "r").readlines()] 
        # Preselected violence terms
        self.violence_list = [elt.strip() for elt in \
                          file(self.txt_path + \
                               "Violentterms.txt", "r").readlines()] 
        # Preselected locations
        self.location_list = [elt.strip() for elt in \
                          file(self.txt_path + \
                               "neighborhoods.txt", "r").readlines()] 

        
    def find_our_hashtags(self):
        """
        Find tweets that contain our hashtags of interest:
        
        #Khialerts
        #Karachi
        #ShiaSunni
        #TargetKilling
        #suicidebombing
        """
        tweet_dict = {}
        primary_key = 0
        for row in \
        self.c.execute("SELECT text FROM tweets WHERE text LIKE"
                       + " '%#Khialerts%'"\
                       + " OR text LIKE '%#Karachi%'" \
                       + " OR text LIKE '%#ShiaSunni%'"\
                       + " OR text LIKE '%#TargetKilling%'"\
                       + " OR text LIKE '%#suicidebombing%'"):
            [row]
            row = row[0].split()
            each_dict = {}
            for word in row:
                each_dict[word] = True
            tweet_dict[primary_key] = each_dict
            primary_key += 1
        return tweet_dict
            
    def violence_score(self):
        """
        Score a tweet based on its inclusion of one or more of
        the following violence terms:

        Gun fire
        Protest
        mob
        target killing
        suicide
        traffic
        """
        # Create dictionary of violence terms
        violence_dict = {term: True for term in self.violence_list}
        # Score each tweet for violence terms
        violence_score = self.find_our_hashtags()
        primary_key = 0
        while primary_key < len(self.find_our_hashtags()):
            # Count of violence terms
            count = 0
            for term in violence_dict.keys():
                if term in violence_score[primary_key]:
                    count += 1
            violence_score[primary_key] = count
            primary_key += 1
        return violence_score
        

    def location_score(self):
        """
        Score a tweet based on its inclusion of one or more of
        the following locations:

        saddar
        clifton
        lyari
        malir
        landhi
        shahraifaisal
        """
        # Create dictionary of violence terms
        location_dict = {term: True for term in self.location_list}
        # Score each tweet for violence terms
        location_score = self.find_our_hashtags()
        primary_key = 0
        while primary_key < len(self.find_our_hashtags()):
            # Count of violence terms
            count = 0
            for term in location_dict.keys():
                if term in location_score[primary_key]:
                    count += 1
            location_score[primary_key] = count
            primary_key += 1
        return location_score

    def total_score(self):
        """
        Calculate the total score for each Tweet based on
        the mention of preselected location and violence
        terms.

        Score = count(violence_terms) *  count(location) /
                count(location)**2
        """
        total_score = self.find_our_hashtags()
        primary_key = 0
        while primary_key < len(total_score):
            for v_score in self.violence_score()[primary_key]:
                for l_score in self.location_score()[primary_key]:
                    t_score = (v_score * l_score)/ l_score**2
            total_score[primary_key] = t_score
        return total_score

    def rank_tweets(self):
        """
        Rank each Tweet as most to least important based on the
        total_score assigned.
        """
        tweets = dict.items(self.total_score())
        sorted_by_second = sorted(tweets, key=lambda tup: tup[1])
        list.reverse(sorted_by_second)
        return sorted_by_second

    def match_tweets(self):
        """
        Match each Tweet to its score
        """
        rank = []
        count = 0
        while count < len(self.rank_tweets()):
            for each_tuple in self.rank_tweets():
                for primary_key in each_tuple[count][0]:
                    if key in self.find_our_hashtags():
                        rank.append(each_tuple[count][1],
                                    self.find_our_hashtags()[key])
        return rank
                
            
        

    
        
