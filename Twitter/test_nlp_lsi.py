from nose import with_setup # optional

# nlp_lsi classes imported for testing
from nlp_lsi import PrepareCorpus, TransformCorpus, SimilarityQueries 

class TestPrepareCorpus:
    '''
    Tests to see if the PrepareCorpus class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")
        prepare_corpus = PrepareCorpus()

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def test_get_data(self):
        '''
        Check to see if data is being loaded from Twitter and
        see how long it's taking
        '''
        pass

    def test_create_documents(self):
        '''
        Check to see if a documents corpus has been created and
        see how long it took
        '''
        pass

    def test_remove_common(self):
        '''
        Check to see if remove_common runs and how long it takes
        '''
        pass

    def test_remove_singular_data_structure(self):
        '''
        Check to see if remove_singluar returns the correct data
        structure
        '''
        pass

    def test_remove_singular_words_working(self):
        '''
        Check to see if singular words have been removed and
        see how long that took
        '''
        pass

    def test_save_dict(self):
        '''
        Check to see if dictionary saved to disk
        '''
        pass

    def test_vector_convert(self):
        '''
        See if the converted document saved to disk as .mm
        '''
        pass


class TestTransformCorpus:
    '''
    Tests to see if the PrepareCorpus class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")
        transform_corpus = TransformCorpus()

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def test_transformation(self):
        '''
        See if .mm converted to tfidf
        '''
        pass

    def test_vectorize_corpus(self):
        '''
        See if the corpus was vectorized
        '''
        pass

    def test_lsi_transform(self):
        '''
        See if the lsi model is working
        '''
        pass

    def test_corpus_lsi(self):
        '''
        Was the whole corpus transformed using LSI?
        '''
        pass

    def test_model_persistency(self):
        '''
        Did the model save to disk correctly?
        '''
        pass


class TestSimilarityQueries:
    '''
    Tests to see if the SimilarityQueries class in nlp_lsi is working
    and checks to see how long it's taking
    '''
    def setup(self):
        print("TestPrepareCorpus: setup() before each test method")

    def teardown(self):
        print("TestPrepareCorpus: teardown() after each test method")

    @classmethod
    def setup_class(cls):
        print("setup_class() before any methods in this class")
        similarity_queries = SimilarityQueries()

    @classmethod
    def teardown_class(cls):
        print("teardown_class() after any methods in this class")

    def test_user_query(self):
        '''
        Print user query and make sure it was transformed properly
        '''
        pass

    def test_init_query_structure(self):
        '''
        This method looks horrible, make sure it has a pulse
        '''
        pass

    def test_perform_query(self):
        '''
        Just make sure a list is returned
        '''
        pass

    def test_sort_similar(self):
        '''
        See how well the query is performing when ranked. Does the queried
        item show up when we know it's there? What order is it ranked?
        '''
        pass

