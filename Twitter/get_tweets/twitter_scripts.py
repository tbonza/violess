
import twitter
import util
from config import *

BOSTON_WOEID = 2367105
api = twitter.Api(consumer_key=key,consumer_secret=secret,access_token_key=access_key,access_token_secret=access_secret)

DEFAULT_RETURN_FIELDS = ['id', 'text', 'created_at']
DEFAULT_USER_RETURN_FIELDS = ['location']


def near(location, radius):
    data = {
        'clifton' : [24.826937,67.031179],
        'karachi' : [24.8600, 67.010]
    }
    location_data = data[location]
    return (location_data[0], location_data[1], "%.3fmi" % radius)

def search(term=None, result_type="recent", return_location=True, return_fields=DEFAULT_RETURN_FIELDS, user_return_fields=DEFAULT_USER_RETURN_FIELDS, lang='en', *args, **kwargs):
    """

use geocode=near(location, radius) to get regional data

ex:
    search("#Karachi", geocode=near('clifton', .5), result_type="recent")


 Args:
      term:
        Term to search by. Optional if you include geocode.
      since_id:
        Returns results with an ID greater than (that is, more recent
        than) the specified ID. There are limits to the number of
        Tweets which can be accessed through the API. If the limit of
        Tweets has occurred since the since_id, the since_id will be
        forced to the oldest ID available. [Optional]
      max_id:
        Returns only statuses with an ID less than (that is, older
        than) or equal to the specified ID. [Optional]
      until:
        Returns tweets generated before the given date. Date should be
        formatted as YYYY-MM-DD. [Optional]
      geocode:
        Geolocation information in the form (latitude, longitude, radius)
        [Optional]
      count:
        Number of results to return.  Default is 15 [Optional]
      lang:
        Language for results as ISO 639-1 code.  Default is None (all languages)
        [Optional]
      locale:
        Language of the search query. Currently only 'ja' is effective. This is
        intended for language-specific consumers and the default should work in
        the majority of cases.
      result_type:
        Type of result which should be returned.  Default is "mixed".  Other
        valid options are "recent" and "popular". [Optional]
      include_entities:
        If True, each tweet will include a node called "entities,".
        This node offers a variety of metadata about the tweet in a
        discrete structure, including: user_mentions, urls, and
        hashtags. [Optional]

    Returns:
      A sequence of twitter.Status instances, one for each message containing
      the term

    """
    data = api.GetSearch(term=term, result_type=result_type, *args, **kwargs)
    dict_data= [datum.AsDict() for datum in data]


    if return_fields == "ALL":
        return dict_data
    parsed_data = []
    for datum in dict_data:
        tweet_dict_data = {}
        for field in return_fields:
            tweet_dict_data[field] = datum.get(field,None)
        if return_location:
            if 'geo' in datum and 'coordinates' in datum['geo']:
                coordinates = datum['geo']['coordinates']
                tweet_dict_data['latitude'] = coordinates[0]
                tweet_dict_data['longitude'] = coordinates[1]
            else:
                tweet_dict_data['latitude'] = None
                tweet_dict_data['longitude'] = None

        parsed_data.append(tweet_dict_data)
    return parsed_data
    #text = [datum.GetText() for datum in data]
    #return data, text

