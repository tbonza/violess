import sqlite3
from gensim import corpora, models, similarities
from collections import Counter

class PrepareCorpus:
    '''
    Prepares the corpus for Latent Semantic Analysis (LSI)
    '''
    def __init__(self):
        # sqlite3
        self.conn = sqlite3.connect('tweet_test.db')
        self.c = self.conn.cursor()
        # File path for saving prepared corpus
        self.path = "/home/ty/code/pakathon/Twitter/"
        # Filter out words from the corpus used with frequency x
        self.frequency = 1

    def create_document_for_lsi(self):
        '''
        Creates a document that is consistent with the format necessary
        for Latent Semantic Indexing (LSI).

        (a) Twitter data is loaded from sqlite3 then a list is made of
            just the Tweets.

            We're assuming that we can ignor the other information like
            primary_key because we know the number of tweets extracted
            from the DB. Given the length, we can later add the other
            information after processing the Tweets to be consistent
            with LSI specifications.

        (b) Given these assumptions, we remove the tweet from the other
            data extracted from the DB.

        (c) To be consistent with LSI, we remove common words such as
            'for a of the and to in'.
        (d) To be consistent with LSI, we remove words that only appear
            once. 
        '''
        tweets = []
        stopdict = {'for':True,'a':True,'of':True,'the':True,'and':True,\
                    'to':True,'in':True}
        word_dict = {}
        plus_one_dict = {}
        # (a) Twitter data is loaded from sqlite3
        for row in self.c.execute("SELECT * FROM tweets;"):
            # Turn query into a list
            [row]
            # (b) Remove tweet from other extracted data
            # Also, split words & lowercase (this is more part of (c))
            tweet = row[3].lower().split()
            tweets.append(tweet)
            for word in tweet:
                # (c) Remove words in Tweet from stoplist
                if word not in stopdict:
                    if word not in word_dict:
                        word_dict[word] = 1
                    # (d) part I, increment words used more than once
                    # for the key in word_dict
                    elif word in word_dict:
                        word_dict[word] += 1
        # This loop returned word_dict, a count of all words used in the
        # tweets, and tweets, a list of tweets where list items are strings.
        
        # (d) part II, words with a count > 1 go into plus_one_dict
        for item in word_dict:
            if item > self.frequency:
                plus_one_dict[item] = True
        # (d) part III, list created that can be passed to next function
        gensim_structure = []
        for submission in tweets:
            one_tweet = ''
            for feature in submission:
                if feature in plus_one_dict:
                    one_tweet = one_tweet + ' ' + feature
            one_tweet = one_tweet.split()
            gensim_structure.append(one_tweet)
        return gensim_structure

    def map_dict(self):
        '''
        Map to dictionary
        '''
        dictionary = corpora.Dictionary(self.create_document_for_lsi())
        return dictionary

    def save_dict(self):
        '''
        Save the dictionary for persistency
        '''
        dictionary = self.map_dict()
        print "save_dict"
        # store the dictionary, for future reference
        dictionary.save(self.path + 'deerwester.dict')
        

    def vector_convert(self):
        '''
        Convert tokenized documents to vectors
        '''
        corpus = [self.map_dict().doc2bow(text) for text
                  in self.create_document_for_lsi()]
        print "vector_convert", corpus[0]
        # store to disk, for later use
        corpora.MmCorpus.serialize(self.path + 'deerwester.mm', corpus)
        # Close the sqlite3 database since each Tweet has been extracted
        self.conn.close()

    def return_the_goods(self):
        '''
        Method assumes that a dictionary hasn't been saved to file
        and that the corpus hasn't been serialzed. Calls both
        methods to complete these tasks.
        '''
        self.save_dict()
        self.vector_convert()
        
        
class TransformCorpus:
    '''
    Transform corpus for LDA or LSI
    '''
    def __init__(self):
        # File path for loading prepared corpus
        self.path = "/home/ty/code/pakathon/Twitter/"
        # Load Dictionary
        self.dictionary = corpora.Dictionary.load(self.path + \
                                                  'deerwester.dict')
        # Load Corpus
        self.corpus = corpora.MmCorpus(self.path + 'deerwester.mm')

    def transformation(self):
        '''
        Create a tfidf transformation
        '''
        # Check to see if the corpus loaded properly
        print "Here's the properly loaded corpus: ", self.corpus
        # Transform using tfidf
        tfidf = models.TfidfModel(self.corpus)
        return tfidf

    def vectorize_corpus(self):
        '''
        Transform vectors for the whole corpus
        '''
        corpus_tfidf = self.transformation()[self.corpus]
        return corpus_tfidf

    def lsi_transform(self):
        '''
        Here we transformed our Tf-Idf corpus via Latent Semantic
        Indexing into a latent 2-D space (2-D because we set num_topics=2).
        '''
        lsi = models.LsiModel(self.vectorize_corpus(),
                              id2word = self.dictionary,
                              num_topics = 2)
        return lsi

    def model_persistency(self):
        '''
        Save the model we created in this class for persistency. In other
        words, so we can use them whenever we want and not have to run
        this class.
        '''
        # Saves LSI model, we could do the same for tfidf, lda...
        self.lsi_transform().save(self.path + 'model.lsi')
        print "LSI model saved"
        
class SimilarityQueries:
    '''
    Obtain similarities of our query document against the indexed documents
    '''
    def __init__(self):
        # File path for loading prepared corpus
        self.path = "/home/ty/code/pakathon/Twitter/"
        # Load LSI model
        self.lsi = models.LsiModel.load(self.path + 'model.lsi')
        # Load Dictionary
        self.dictionary = corpora.Dictionary.load(self.path + \
                                                  'deerwester.dict')
        # Load Corpus
        self.corpus = corpora.MmCorpus(self.path + 'deerwester.mm')
        # Activate sqlite3 database
        self.conn = sqlite3.connect('tweet_test.db')

    def user_query(self):
        '''
        Let's query #theirfanism
        '''
        doc = "#theirfanism"
        vec_bow = self.dictionary.doc2bow(doc.lower().split())
        vec_lsi = self.lsi[vec_bow]
        return vec_lsi

    def init_query_structure(self):
        '''
        To prepare for similarity queries, we need to enter all documents
        which we want to compare against subsequent queries. In our case,
        they are the same nine documents used for training LSI, converted
        to 2-D LSA space.
        '''
        # transform corpus to LSI space and index it
        index = similarities.MatrixSimilarity(self.lsi[self.corpus])
        
        # Save index for persistency
        # index.save(self.path + 'deerwester.index')
        
        # Here we're not going to load the index after saving, it's going
        # to be kept in memory.
        # If we want to change that later then it's
        # index = similarities.MatrixSimilarity.load(self.path + \
        #                                           'deerwester.index')
        return index

    def perform_query(self):
        '''
        Perform a similarity query against the corpus
        '''
        sims = self.init_query_structure()[self.user_query()]
        return sims

    def sort_similar(self):
        '''
        Sort the similarities into descending order
        '''
        sim_sort = sorted(enumerate(self.perform_query()), \
                          key = lambda item: -item[1])
        return sim_sort

    def match_tweet_to_sim(self):
        '''
        Matches the tweet to the similarity score of the tweet corpus
        for the term we searched.

        The example uses #theirfanism
        '''
        # Initialize PrepareCorpus to get Twitter data
        prepare_corpus = PrepareCorpus()
        # Put twitter data in a dictionary 
        tweet_dict = {}
        count = 0
        for tweet in prepare_corpus.create_document_for_lsi():
            tweet_dict[count] = tweet
            count += 1
        # Given that we know the number of Tweets extracted from the
        # database it follows that the number of documents we analyzed
        # will be the same.
        #
        # Secondly, the gensim method originally put each Tweet in a
        # separate dictionary where the Key is the order in which the
        # Tweet was extracted from the database.
        #
        # Therefore, we can match the original Tweet to the similarity
        # score it was assigned when compared to the phrase the user
        # queried. The sort_similar method returns a tuple of
        # (gensim dictionary key, similarity score).
        match_list = []
        for row in self.sort_similar():
            [row]
            for num in tweet_dict.keys():
                if num == row[0]:
                    match_list.append(tweet_dict[num])
        return match_list

    def print_matches(self):
        '''
        Print the first five matches
        '''
        print self.match_tweet_to_sim()[0:4]
        # Close database here even though it's the wrong
        # thing to do
        self.conn.close()


