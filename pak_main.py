import Twitter
import time

try:
    prepare_corpus = Twitter.nlp_lsi.PrepareCorpus()
    print("PrepareCorpus successfully initialized")
    prepare_corpus.return_the_goods()
    print("return_the_goods successfully called")
except:
    print("problems with PrepareCorpus")

try:
    transform_corpus = Twitter.nlp_lsi.TransformCorpus()
    print("TransformCorpus successfully initialized")
    transform_corpus.lsi_transform().print_topics()
    print("Here's the list of topics LSI thinks that the"\
          + " Tweets share in common")
    transform_corpus.model_presistency()
    print("model_persistency successfully called")
except:
    print("problems with TransformCorpus")



