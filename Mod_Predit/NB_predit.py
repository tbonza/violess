import numpy as np
from sklearn import naive_bayes

data = np.genfromtxt('data.csv', delimiter=',')
X_train = data[:-20]
X_test = data[-20:]
target = np.genfromtxt('target.csv', delimiter=',', dtype=str)
Y_train = target[:-20]
Y_test = target[-20:]
mnb = naive_bayes.MultinomialNB()
mnb.fit(X_train, Y_train)
print(mnb.predict(X_test))
print(Y_test)

